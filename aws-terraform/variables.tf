variable "profile" {
  default = "default"
  description = "AWS profile"
  type        = string
}

variable "region" {
  description = "AWS region to deploy to"
  default = "us-east-2"
  type        = string
}

variable "cluster_name" {
  description = "test"
  default = "test"
  type = string
}

variable "availability_zones" {
  type  = list(string)
  default = ["us-east-2a", "us-east-2b"]
  description = "List of availability zones for the selected region"
}
