terraform {
  backend "s3" {
    bucket = "aws-eks-cluster-terraform-state"
    key = "aws-eks-cluster/terraform.tfstate"
    region = "us-east-2"
    dynamodb_table = "aws-eks-cluster-tf-state"
    encrypt = true
  }
}
